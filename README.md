# Welcome
Welcome to the CosmicCord Master Documentation

The Master branch ss constantly updated and is always a work in progress, we're constantly adding new features so keep an eye out.
This is classed as bleeding edge and can possibly contain breaking changes at a moments notice

## About
CosmicCord is sleek, fast and powerful [Discord API](https://discordapp.com/developers/docs/intro) library written in [TypeScript](https://www.typescriptlang.org/) that is aimed to provide the most performance possible with the most features.

## Installation
>**Node.js 10.0.0 or newer is required.**  
>[node-gyp](https://www.npmjs.com/package/node-gyp) is recommended but not needed.
>Ignore any warnings about unmet peer dependencies, as they're all optional.

Installation is as easy as
- npm
`npm install https://gitlab.com/osmiumdev/cosmiccord.git`
 - yarn
 `yarn add https://gitlab.com/osmiumdev/cosmiccord.git`

>CosmicCord will attempt to install [cws](https://www.npmjs.com/package/@clusterws/cws)
>If they fail, they will fallback to [ws](https://www.npmjs.com/package/ws). Installation will continue as normal

### Optional packages

- [zlib-sync](https://www.npmjs.com/package/zlib-sync) for significantly faster WebSocket data inflation (`npm install zlib-sync`)

- [erlpack](https://github.com/yuwui/erlpack) for significantly faster WebSocket data (de)serialisation (`npm install https://github.com/yuwui/erlpack`)

## Example usage
- TypeScript
>```ts
> import { Client, DiscordEvents, Message } from 'cosmiccord';
>
> const bot = new Client('token');
> bot.on(DiscordEvents.READY, () => console.log(`${bot.self.tag} is ready with ${bot.guilds.size} guilds.`));
> bot.on(DiscordEvents.MESSAGE_CREATE, (message: Message) => {
>   if (message.content == '!ping') message.return(`Pong! ${message.guild.shard.ping} ms`);
> })
>```

- JavaScript
>```js
> const { Client, DiscordEvents } = require('cosmiccord');
>
> const bot = new Client('Token');
> bot.on(DiscordEvents.READY, () => console.log(`${bpt.self.tag} is ready with ${bot.guilds.size} guilds.`));
> bot.on(DiscordEvents.READY, message => {
>   if (message.content == '!ping') message.return(`Pong! ${message.guild.shard.ping} ms`);
> });
>```

## Links
* [Documentation](#)
* [Guide](#)
* [Osmium Development Discord Server](https://discord.gg/asQ8GUh)
* [Discord API Discord server](https://discord.gg/discord-api)
* [GitLab](https://gitlab.com/osmiumdev/cosmiccord)
* [Related libraries](https://discordapi.com/unofficial/libs.html)

## Contributing
Before creating an issue, please ensure that it hasn't already been reported/suggested, and double-check the
[documentation](http://docs.osmiumevolution.com/#/cosmiccord/master/general/welcome).

## Help
If you don't understand something in the documentation, you are experiencing problems or you just need a gentle
nudge in the right direction, please don't hesitate to join our official and ask in the CosmicCord section [Osmium Development Discord Server](https://discord.gg/asQ8GUh).
