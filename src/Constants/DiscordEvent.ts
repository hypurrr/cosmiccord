export default {
  /**
   * Emits warnings thrown by the client
   * @event Client#WARN
   * @param {string} warn The warning being emitted
   */
  WARN: 'WARN',
  /**
   * Emits all errors caught by the client
   * @event Client#ERROR
   * @param {Error|TypeError|RangeError|Error} error The event being emitted
   */
  ERROR: 'ERROR',
  /**
   * Internal debugging for CosmicCord, If you have any issues use this to follow the process
   * @event Client#DEBUG
   * @param {any} debug The data being debugged
   */
  DEBUG: 'DEBUG',
  /**
   * Emitted when the client hits a rate limit while making a request
   * @event Client#RATE_LIMIT
   * @param {Object} rateLimitInfo Object containing the rate limit info
   * @param {number} rateLimitInfo.timeout Timeout in ms
   * @param {number} rateLimitInfo.limit Number of requests that can be made to this endpoint
   * @param {string} rateLimitInfo.method HTTP method used for request that triggered this event
   * @param {string} rateLimitInfo.path Path used for request that triggered this event
   * @param {string} rateLimitInfo.route Route used for request that triggered this event
   */
  RATE_LIMIT: 'RATE_LIMIT',
  /**
   * Emits events the events directly from the gateway
   * @event Client#RAW
   * @param {any} packet The raw event packet received from the gateway
   */
  RAW: 'RAW',
  /**
   * Emitted once the shard is ready
   * @event Client#SHARD_READY
   * @param {Shard} shard The current shard
   */
  SHARD_READY: 'SHARD_READY',
  /**
   * Emitted when the Shard is reconnecting
   * @event Client#SHARD_RECONNECT
   * @param {Object} [shard] Object containing the Shard
   * @param {string} [shard.id] The Shards ID
   */
  SHARD_RECONNECT: 'SHARD_RECONNECT',
  /**
   * Fired when a shard resumes
   * @event Client#SHARD_RESUME
   * @param {number} id The ID of the shard
   */
  SHARD_RESUME: 'SHARD_RESUME',
  /**
   * Emitted once a Shard is loading, inner data is a partial Shard Data
   * @event Client#SHARD_LOADING
   * @param {string} id The id of the shard
   */
  SHARD_LOADING: 'SHARD_LOADING',
  /**
   * Emitted when a shard disconnects
   * @event Client#SHARD_DISCONNECT
   * @prop {Error?} error The error, if any
   * @prop {number} id The ID of the shard
   */
  SHARD_DISCONNECT: 'SHARD_DISCONNECT',
  /**
   * Emitted once all shards disconnect
   * @event Client#SHARD_DISCONNECT_ALL
   */
  SHARD_DISCONNECT_ALL: 'SHARD_DISCONNECT_ALL',
  /**
   * Emitted once a Shard Disconnects
   * @event Client#DISCONNECT
   * @param {Shard} shard Partial Shard data
   * @param {string} shard.id Id of the shard
   * @param {string} shard.description Description of the disconnect
   * @param {string} shard.reason Websocket reason for the disconnect
   */
  DISCONNECT: 'DISCONNECT',
  /**
   * Emitted once the client is ready
   * @event Client#READY
   */
  READY: 'READY',
  /**
   * Emitted once a bot joins a guild
   * @event Client#GUILD_CREATE
   * @param {Guild} guild The guild that became available
   */
  GUILD_CREATE: 'GUILD_CREATE',
  /**
   * Emitted once a guild becomes available
   * @event Client#GUILD_AVAILABLE
   * @param {Guild} guild The guild that became available
   */
  GUILD_AVAILABLE: 'GUILD_AVAILABLE',
  /**
   * Emitted once the bot leaves a Guild
   * @event Client#GUILD_DELETE
   * @param {Guild} guild The guild that has been deleted
   */
  GUILD_DELETE: 'GUILD_DELETE',
  /**
   * Emitted once a User updates his Status
   * @event Client#GUILD_EMOJI_UPDATE
   * @param {Guild} newGuild The new guild data
   * @param {Guild} oldGuild The old guild data
   */
  GUILD_EMOJI_UPDATE: 'GUILD_EMOJI_UPDATE',
  /**
   * Emitted when a channel pin timestamp is updated
   * @event Client#CHANNEL_PINS_UPDATE
   * @param {TextChannel} channel The channel
   * @param {number} timestamp The new timestamp
   * @param {number} oldTimestamp The old timestamp
   */
  CHANNEL_PINS_UPDATE: 'CHANNEL_PINS_UPDATE',
  /**
   * Emitted when a channel's webhooks are updated
   * @event Client#WEBHOOKS_UPDATE
   * @param {Object} data The update data
   * @param {string} channelID The ID of the channel that webhooks were updated in
   * @param {string} guildID The ID of the guild that webhooks were updated in
   */
  WEBHOOKS_UPDATE: 'WEBHOOKS_UPDATE',
  /**
   * Emitted once a User updates his/her info
   * @event Client#USER_UPDATE
   * @param {User} newUser The new data of the user ( Present in cache )
   * @param {User} oldUser The old data of the user ( Not present in cache )
   */
  USER_UPDATE: 'USER_UPDATE',
  /**
   * Emitted once a User updates his Status
   * @event Client#PRESENCE_UPDATE
   * @param {Object} [old_data] The old presence data
   * @param {string} [old_data.status] The old status
   * @param {Object?} [old_data.game] The old activity data
   * @param {Object} [new_data] The new presence data
   * @param {string} [new_data.status] The new status
   * @param {Object?} [new_data.game] The new activity data
   */
  PRESENCE_UPDATE: 'PRESENCE_UPDATE',
  /**
   * Emitted once a message is seen/sent
   * @event Client#MESSAGE_CREATE
   * @param {Message} message
   */
  MESSAGE_CREATE: 'MESSAGE_CREATE',
  /**
   * Emitted when a message is updated
   * @event Client#MESSAGE_UPDATE
   * @param {Message} message The new content of the message
   * @param {Message|Object} oldMessage The old message, if it is not cached only id and client will be provided
   */
  MESSAGE_UPDATE: 'MESSAGE_UPDATE',
  /**
   * Emitted when a message is deleted
   * @event Client#MESSAGE_DELETE
   * @param {Message} message
   */
  MESSAGE_DELETE: 'MESSAGE_DELETE',
  /**
   * Emitted when a bulk delete occurs
   * @event Client#MESSAGE_DELETE_BULK
   * @param {Message[] | Object[]} messages An array of (potentially partial) message objects. If a message is not cached, it will be an object with `id` and `channel` keys.
   */
  MESSAGE_DELETE_BULK: 'MESSAGE_DELETE_BULK',
  /**
   * Emitted when someone adds a reaction to a message
   * @event Client#MESSAGE_REACTION_ADD
   * @param {Message | Object} message The message object. If the message is not cached, this will be an object with `id` and `channel` keys. No other property is guaranteed
   * @param {Object} [emoji] The reaction emoji object
   * @param {string?} [emoji.id] The emoji ID (null for non-custom emojis)
   * @param {string} [emoji.name] The emoji name
   * @param {string} userID The ID of the user that added the reaction
   */
  MESSAGE_REACTION_ADD: 'MESSAGE_REACTION_ADD',
  /**
   * Emitted when someone removes a reaction from a message
   * @event Client#MESSAGE_REACTION_REMOVE
   * @param {Message | Object} message The message object. If the message is not cached, this will be an object with `id` and `channel` keys. No other property is guaranteed
   * @param {Object} [emoji] The reaction emoji object
   * @param {string?} [emoji.id] The ID of the emoji (null for non-custom emojis)
   * @param {string} [emoji.name] The emoji name
   * @param {string} userID The ID of the user that removed the reaction
   */
  MESSAGE_REACTION_REMOVE: 'MESSAGE_REACTION_REMOVE',
  /**
   * Emitted when someone removes a reaction from a message
   * @event Client#MESSAGE_REACTION_REMOVE_ALL
   * @param {Message | Object} message The message object. If the message is not cached, this will be an object with `id` and `channel` keys. No other property is guaranteed
   */
  MESSAGE_REACTION_REMOVE_ALL: 'MESSAGE_REACTION_REMOVE_ALL',
  /**
   * Emitted when a user begins typing
   * @event Client#TYPING_START
   * @param {DMChannel | TextChannel} channel The text channel the user is typing in
   * @param {User} user The user
   */
  TYPING_START: 'TYPING_START',
  /**
   * Emitted when a user joins a guild
   * @event Client#GUILD_MEMBER_ADD
   * @param {Member} member The member object that joined
   * @param {Guild} guild The guild the member that joined
   */
  GUILD_MEMBER_ADD: 'GUILD_MEMBER_ADD',
  /**
   * Emitted when a user leaves a guild
   * @event Client#GUILD_MEMBER_REMOVE
   * @param {Member} member The member object that left
   * @param {Guild} guild The guild the member that left
   */
  GUILD_MEMBER_REMOVE: 'GUILD_MEMBER_REMOVE',
  /**
   * Emitted when a member is updated
   * @event Client#GUILD_MEMBER_UPDATE
   * @param {Member} member The new member data ( Cached )
   * @param {Member} oldMember The old member data ( Partial, not cached )
   * @param {string?} oldMember.nick The old nickname of the member
   * @param {Collection} oldMember.roles The old roles of the member
   */
  GUILD_MEMBER_UPDATE: 'GUILD_MEMBER_UPDATE',
  /**
   * Emitted when a user is banned from a guild
   * @event Client#GUILD_BAN_ADD
   * @param {Guild} guild The guild
   * @param {User} user The banned user
   */
  GUILD_BAN_ADD: 'GUILD_BAN_ADD',
  /**
   * Emitted when a user is unbanned from a guild
   * @event Client#GUILD_BAN_REMOVE
   * @param {Guild} guild The guild
   * @param {User} user The banned user
   */
  GUILD_BAN_REMOVE: 'GUILD_BAN_REMOVE',
  /**
   * Emitted once a role is created
   * @event Client#ROLE_CREATE
   * @param {Role} role The role that was created
   */
  ROLE_CREATE: 'ROLE_CREATE',
  /**
   * Emitted once a role is deleted, this also emits one or multiple `Client#GUILD_MEMBER_UPDATE`
   * @event Client#ROLE_DELETE
   * @param {Role} role The deleted role
   */
  ROLE_DELETE: 'ROLE_DELETE',
  /**
   * Emitted once a role gets updated
   * @event Client#ROLE_UPDATE
   * @param {Role} oldRole The old role data
   * @param {Role} role The new role data
   */
  ROLE_UPDATE: 'ROLE_UPDATE',
  /**
   * Emitted once a channel has been created
   * @event Client#CHANNEL_CREATE
   * @param {DMChannel|TextChannel|VoiceChannel|CategoryChannel|NewsChannel|StoreChannel}
   */
  CHANNEL_CREATE: 'CHANNEL_CREATE',
  /**
   * Emitted once a channel gets updated
   * @event Client#CHANNEL_UPDATE
   * @param {DMChannel|TextChannel|VoiceChannel|CategoryChannel|NewsChannel|StoreChannel} oldChannel The old data of the channel ( Not Cached )
   * @param {DMChannel|TextChannel|VoiceChannel|CategoryChannel|NewsChannel|StoreChannel} newChannel The new data of the channel
   */
  CHANNEL_UPDATE: 'CHANNEL_UPDATE',
  /**
   * Emitted once a channel gets deleted
   * @event Client#CHANNEL_DELETE
   * @param {DMChannel|TextChannel|VoiceChannel|CategoryChannel|NewsChannel|StoreChannel} channel The data of the deleted channel
   */
  CHANNEL_DELETE: 'CHANNEL_DELETE',
  /**
   * Voice server update, unknown data yet
   * @event Client#VOICE_SERVER_UPDATE
   */
  VOICE_SERVER_UPDATE: 'VOICE_SERVER_UPDATE',
  /**
   * Emitted whenever a member changes voice state - e.g. joins/leaves a channel, mutes/unmutes.
   * @event Client#VOICE_STATE_UPDATE
   * @param {VoiceState?} oldState The voice state before the update
   * @param {VoiceState} newState The voice state after the update
   */
  VOICE_STATE_UPDATE: 'VOICE_STATE_UPDATE',
  /**
   * Emitted whenever a stream Dispatcher subscribes to the broadcast.
   * @event VoiceBroadcast#VOICE_BROADCAST_SUBSCRIBE
   * @param {StreamDispatcher} subscriber The subscribed Dispatcher
   */
  VOICE_BROADCAST_SUBSCRIBE: 'VOICE_BROADCAST_SUBSCRIBE',
  /**
   * Emitted whenever a stream Dispatcher unsubscribes to the broadcast.
   * @event VoiceBroadcast#VOICE_BROADCAST_UNSUBSCRIBE
   * @param {StreamDispatcher} Dispatcher The unsubscribed Dispatcher
   */
  VOICE_BROADCAST_UNSUBSCRIBE: 'VOICE_BROADCAST_UNSUBSCRIBE',
};
