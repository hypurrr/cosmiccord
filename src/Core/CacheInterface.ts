import RedisClient, { Redis } from 'ioredis';
import { Client } from './Client';
import { RedisMap } from '../Models/RedisMap';

function returnedIteratedArray(map: Map<string, any>, iterable: IterableIterator<any>, count?: number) {
  if (count === undefined) return iterable.next().value;
  if (!Number.isInteger(count) || count < 1) throw new RangeError('The count must be an integer greater than 0.');
  count = Math.min(map.size, count);
  const arr = new Array(count);
  for (let i = 0; i < count; i++) arr[i] = iterable.next().value;
  return arr;
}

export class CacheInterface {
  private client: Client;
  private redis: Redis | RedisMap;
  public constructor(client: Client) {
    this.client = client;
    if (!this.client.options.redis.enable) {
      this.redis = new RedisMap();
    } else {
      this.redis = RedisClient(this.client.options.redis.port, this.client.options.redis.host)
    }
  }

  async get(i: string): Promise<any> {
    try {
      const reply = await this.redis.get(i);
      if (reply === null) throw new Error();
      return JSON.parse(reply);
    } catch (err) {
      throw err;
    }
  }

  async set(i: string, data: any) {
    try {
      const reply = await this.redis.set(i, JSON.stringify(data));
      if (reply === null) throw new Error();
      return reply;
    } catch (err) {
      throw err;
    }
  }

  async del(i: string) {
    try {
      const reply = await this.redis.del(i);
      if (reply === null) throw new Error();
      if (reply === 1) return true;
      else return false;
    } catch (err) {
      throw err;
    }
  }

  async has(i: string) {
    try {
      return !!await this.get(i);
    } catch (_) {
      return false;
    }
  }

  async map(index = '*'): Promise<Map<string, any>> {
    try {
      const map = new Map();
      const keys = await this.redis.keys(index);
      if (keys) {
        for (let i = 0; i < keys.length; i++) {
          map.set(keys[i].replace(index.replace('*', ':'), ''), await this.get(keys[i]));
        }
      }
      return map
    } catch (err) {
      throw err;
    }
  }

  async find(fn: (value: any, key?: string, cache?: any) => boolean, index?: string) {
    const map = await this.map(index);
    for (const [key, val] of map) {
      if (fn(val, key, this)) return val;
    }
    return null;
  }

  async findKey(fn: (value: any, key?: string, cache?: any) => boolean, index?: string) {
    const map = await this.map(index);
    for (const [key, val] of map) {
      if (fn(val, key, this)) return key;
    }
    return null;
  }

  async first(count?: number, index?: string) {
    const map = await this.map(index);
    return returnedIteratedArray(map, map.values(), count);
  }

  async firstKey(count?: number, index?: string) {
    const map = await this.map(index);
    return returnedIteratedArray(map, map.keys(), count);
  }
}
