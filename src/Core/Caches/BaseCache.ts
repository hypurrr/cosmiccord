import { Client } from '../Client';
import { CacheInterface } from '../CacheInterface';

export abstract class BaseCache {
  public readonly client: Client;
  public readonly cache: CacheInterface;
  private _array?: Array<string>;
  private _keyArray?: Array<string>;
  public readonly endpoint: any;
  public readonly class: any;
  constructor(client: Client, endpoint: any, useClass: any) {
    this.client = client;
    this.cache = this.client.cache;
    this.endpoint = endpoint;
    this.class = useClass;
  }
  async get(id: string) {
    let data = await this.cache.get(`${this.class.name}:${id}`);
    if (data === null) {
      data = await this.client.rest.request('get', this.endpoint(id));
      this.set(id, data);
    }
    return new this.class(this.client, data);
  }

  async set(id: string, data: any) {
    if (await this.cache.get(`${this.class.name}:${id}`) !== null) {
      await this.cache.del(`${this.class.name}:${id}`);
    }
    return await this.cache.set(`${this.class.name}:${id}`, data);
  }

  has(id: string) {
    return this.cache.has(`${this.class.name}:${id}`);
  }

  map() {
    return this.cache.map(`${this.class.name}:*`);
  }

  find(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.find(fn, `${this.class.name}:*`);
  }

  findKey(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.findKey(fn, `${this.class.name}:*`);
  }

  async array(): Promise<Array<string>> {
    const map = await this.map();
    if (!this._array || this._array.length !== map.size) this._array = [...map.values()];
    return this._array;
  }

  async keyArray(): Promise<Array<string>> {
    const map = await this.map();
    if (!this._keyArray || this._keyArray.length !== map.size) this._keyArray = [...map.keys()];
    return this._keyArray;
  }

  first(count?: number) {
    return this.cache.first(count, `${this.class.name}:*`);
  }

  firstKey(count?: number) {
    return this.cache.firstKey(count, `${this.class.name}:*`);
  }
}
