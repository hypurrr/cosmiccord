import { Client } from '../Client';
import Endpoints from '../Rest/Endpoints';
import { Channel } from '../../Models/Channel';
import { BaseCache } from './BaseCache';
import { TextChannel } from '../../Models/Guild/Channels/TextChannel';
import { VoiceChannel } from '../../Models/Guild/Channels/VoiceChannel';
import { CategoryChannel } from '../../Models/Guild/Channels/CategoryChannel';
import { NewsChannel } from '../../Models/Guild/Channels/NewsChannel';
import { StoreChannel } from '../../Models/Guild/Channels/StoreChannel';
import { DMChannel } from '../../Models/DMChannel';

export class ChannelCache extends BaseCache {
  constructor(client: Client) {
    super(client, Endpoints.CHANNEL, Channel);
  }

  async get(id: string) {
    let data = await this.cache.get(`${this.class.name}:*:${id}`);
    if (data === null) {
      data = await this.client.rest.request('get', this.endpoint(id));
      await this.set(`${data.guild_id || data.owner_id || 'unknown'}:${id}`, data);
    }
    if (data.guild_id) {
      const guild = await this.client.guilds.get(data.guild_id);
      switch (data.type) {
        case 0:
          return new TextChannel(guild, data);
        case 2:
          return new VoiceChannel(guild, data);
        case 4:
          return new CategoryChannel(guild, data);
        case 5:
          return new NewsChannel(guild, data);
        case 6:
          return new StoreChannel(guild, data);
        default:
          return new Channel(this.client, data);
      }
    } else if (data.owner_id) {
      const user = await this.client.users.get(data.owner_id);
      if (data.type === 1) return new DMChannel(user, data);
      else return new Channel(this.client, data);
    } else {
      return new Channel(this.client, data);
    }
  }

  async set(id: string, data: any) {
    const channel = await this.client.rest.request('get', this.endpoint(id));
    return super.set(`${channel.guild_id || channel.owner_id || 'unknown'}:${id}`, data);
  }

  map() {
    return this.cache.map(`${this.class.name}:*:*`);
  }
  find(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.find(fn, `${this.class.name}:*:*`);
  }

  findKey(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.findKey(fn, `${this.class.name}:*:*`);
  }

  first(count?: number) {
    return this.cache.first(count, `${this.class.name}:*:*`);
  }

  firstKey(count?: number) {
    return this.cache.firstKey(count, `${this.class.name}:*:*`);
  }
}
