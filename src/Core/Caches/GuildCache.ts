import { Client } from '../Client';
import Endpoints from '../Rest/Endpoints';
import { Guild } from '../../Models/Guild/Guild';
import { BaseCache } from './BaseCache';

export class GuildCache extends BaseCache {
  constructor(client: Client) {
    super(client, Endpoints.GUILD, Guild);
  }
  get(id: string): Promise<Guild> {
    return this.get(id);
  }
}
