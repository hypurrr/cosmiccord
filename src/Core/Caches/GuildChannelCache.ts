import { Client } from '../Client';
import Endpoints from '../Rest/Endpoints';
import { Channel } from '../../Models/Channel';
import { BaseCache } from './BaseCache';
import { TextChannel } from '../../Models/Guild/Channels/TextChannel';
import { Guild } from '../../Models/Guild/Guild';
import { VoiceChannel } from '../../Models/Guild/Channels/VoiceChannel';
import { CategoryChannel } from '../../Models/Guild/Channels/CategoryChannel';
import { NewsChannel } from '../../Models/Guild/Channels/NewsChannel';
import { StoreChannel } from '../../Models/Guild/Channels/StoreChannel';

export class GuildChannelCache extends BaseCache {
  public guild: Guild;
  constructor(client: Client, guild: Guild) {
    super(client, Endpoints.GUILD_CHANNEL, Channel);
    this.guild = guild;
  }

  async get(id: string) {
    let data = await this.cache.get(`${this.class.name}:${this.guild.id}:${id}`);
    if (data === null) {
      data = await this.client.rest.request('get', this.endpoint(this.guild.id, id));
      await this.set(id, data);
    }
    switch (data.type) {
      case 0:
        return new TextChannel(this.guild, data);
      case 2:
        return new VoiceChannel(this.guild, data);
      case 4:
        return new CategoryChannel(this.guild, data);
      case 5:
        return new NewsChannel(this.guild, data);
      case 6:
        return new StoreChannel(this.guild, data);
      default:
        return new Channel(this.client, data);
    }
  }

  set(id: string, data: any) {
    return super.set(`${this.guild.id}:${id}`, data);
  }

  map() {
    return this.cache.map(`${this.class.name}:${this.guild.id}:*`);
  }
  find(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.find(fn, `${this.class.name}:${this.guild.id}:*`);
  }

  findKey(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.findKey(fn, `${this.class.name}:${this.guild.id}:*`);
  }

  first(count?: number) {
    return this.cache.first(count, `${this.class.name}:${this.guild.id}:*`);
  }

  firstKey(count?: number) {
    return this.cache.firstKey(count, `${this.class.name}:${this.guild.id}:*`);
  }
}
