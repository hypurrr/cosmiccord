import { BaseCache } from './BaseCache';
import { Client } from '../Client';
import Endpoints from '../Rest/Endpoints';
import { Message } from '../../Models/Message';
import { TextChannel } from '../../Models/Guild/Channels/TextChannel';
import { Channel } from '../../Models/Channel';

export class MessageCache extends BaseCache {
  public channel: TextChannel | Channel;
  constructor(client: Client, channel: TextChannel | Channel) {
    super(client, Endpoints.CHANNEL_MESSAGE, Message);
    this.channel = channel;
  }
  get(id: string) {
    return super.get(`${this.channel.id}:${id}`);
  }

  set(id: string, data: any) {
    return super.set(`${this.channel.id}:${id}`, data);
  }

  has(id: string) {
    return super.has(`${this.channel.id}:${id}`);
  }

  map() {
    return this.cache.map(`${this.class.name}:${this.channel.id}:*`);
  }

  find(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.find(fn, `${this.class.name}:${this.channel.id}:*`);
  }

  findKey(fn: (value: any, key?: string, cache?: any) => boolean) {
    return this.cache.findKey(fn, `${this.class.name}:${this.channel.id}:*`);
  }

  first(count?: number) {
    return this.cache.first(count, `${this.class.name}:${this.channel.id}:*`);
  }

  firstKey(count?: number) {
    return this.cache.firstKey(count, `${this.class.name}:${this.channel.id}:*`);
  }
}
