import { BaseCache } from './BaseCache';
import { Client } from '../Client';
import Endpoints from '../Rest/Endpoints';
import { User } from '../../Models/User';

export class UserCache extends BaseCache {
  constructor(client: Client) {
    super(client, Endpoints.USER, User);
  }
}