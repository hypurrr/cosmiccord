import { Core } from './Core';
import { RestManager } from './Rest/RestManager';
import { CacheInterface } from './CacheInterface';
import { UserCache } from './Caches/UserCache';
import { ShardManager } from './Gateway/ShardManager';
import DiscordEvent from '../Constants/DiscordEvent';
import { SelfUser } from '../Models/SelfUser';
import { GuildCache } from './Caches/GuildCache';
import {ChannelCache} from "./Caches/ChannelCache";

/**
 * Client
 * @class {Client}
 * @extends {Core}
 * @param {string} token The token of your bot application
 * @param {*} options Options for the client to use
 */
export class Client extends Core {
  public options: any;
  public token = '';
  public rest: RestManager;
  public cache: CacheInterface;
  public users: UserCache;
  public guilds: GuildCache;
  public startTime?: number;
  public ready = false;
  public shardID = 0;
  public shards: ShardManager;
  public presence: any;
  public self?: SelfUser;
  public channels: ChannelCache;

  public constructor(token: string, options?: any) {
    super();
    Object.defineProperty(this, 'options', { value: options, enumerable: false });
    Object.defineProperty(this, 'token', { value: token, enumerable: false });
    this.rest = new RestManager(this);
    this.cache = new CacheInterface(this);
    this.users = new UserCache(this);
    this.guilds = new GuildCache(this);
    this.channels = new ChannelCache(this);
    this.shards = new ShardManager(this);
  }

  public shard(id?: number) {
    return { id: id || this.shardID } as any;
  }

  public async connect() {
    if (!this.options.ws.enabled) {
      this.emit(DiscordEvent.DEBUG, [Client, 'Gateway is disabled, Starting instantly']);
      this.ready = true;
    } else {
      try {
        this.emit(DiscordEvent.DEBUG, [Client, 'Attempting to initialise the Shard Manager']);
        await this.shards._init();
      } catch (err) {
        this.emit(DiscordEvent.ERROR, [Client, 'Unable to initialise the Shard Manager']);
        this.destroy();
        throw err;
      }
    }
  }
}
