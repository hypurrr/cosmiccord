import EventEmitter from 'eventemitter3';
import Timeout = NodeJS.Timeout;
import Immediate = NodeJS.Immediate;
import { setInterval } from 'timers';

export declare interface Core extends EventEmitter {
  subscribe(event: string, listener: Function): this;
}

/**
 * The Core of CosmicCord
 * @class Core
 */
export class Core extends EventEmitter {
  private _timeouts: Set<Timeout>;
  private _intervals: Set<Timeout>;
  private _immediates: Set<Immediate>;
  constructor() {
    super();
    /**
     * Timeouts set by {@link Core.setTimeout} that are still active
     * @type {Set<NodeJS.Timeout>}
     * @private
     */
    this._timeouts = new Set();
    /**
     * Intervals set by {@link Core.setInterval} that are still active
     * @type {Set<NodeJS.Timeout>}
     * @private
     */
    this._intervals = new Set();
    /**
     * Intervals set by {@link Core.setImmediate} that are still active
     * @type {Set<NodeJS.Immediate>}
     * @private
     */
    this._immediates = new Set();
  }

  /**
   * Destroys all timers set by the core
   */
  public destroy() {
    for (const x of this._timeouts) this.clearTimeout(x);
    for (const x of this._intervals) this.clearInterval(x);
    for (const x of this._immediates) this.clearImmediate(x);
    this._timeouts.clear();
    this._intervals.clear();
    this._immediates.clear();
  }

  /**
   * Sets a timeout that will be automatically cancelled if the client is destroyed.
   * @param {Function} fn Function to execute
   * @param {number} delay Time to wait before executing (in milliseconds)
   * @param {...*} args Arguments for the function
   * @returns {NodeJS.Timeout}
   */
  public setTimeout(fn: Function, delay: number, ...args: Array<unknown>) {
    const timeout = setTimeout(() => {
      fn(...args);
      this._timeouts.delete(timeout);
    }, delay);
    this._timeouts.add(timeout);
    return timeout;
  }

  /**
   * Clears a timeout.
   * @param {Timeout} timeout Timeout to cancel
   */
  public clearTimeout(timeout: Timeout) {
    clearTimeout(timeout);
    this._timeouts.delete(timeout);
  }

  /**
   * Sets an interval that will be automatically cancelled if the client is destroyed.
   * @param {Function} fn Function to execute
   * @param {number} delay Time to wait between executions (in milliseconds)
   * @param {...*} args Arguments for the function
   * @returns {NodeJS.Timeout}
   */
  public setInterval(fn: (...args: Array<unknown>) => void, delay: number, ...args: Array<unknown>): Timeout {
    const interval = setInterval(fn, delay, ...args);
    this._intervals.add(interval);
    return interval;
  }

  /**
   * Clears an interval.
   * @param {Timeout} interval Interval to cancel
   */
  public clearInterval(interval: Timeout) {
    clearInterval(interval);
    this._intervals.delete(interval);
  }

  /**
   * Sets an immediate that will be automatically cancelled if the client is destroyed.
   * @param {Function} fn Function to execute
   * @param {...*} args Arguments for the function
   * @returns {Immediate}
   */
  public setImmediate(fn: (...args: Array<unknown>) => void, ...args: any) {
    const immediate = setImmediate(fn, ...args);
    this._immediates.add(immediate);
    return immediate;
  }

  /**
   * Clears an immediate.
   * @param {Immediate} immediate Immediate to cancel
   */
  public clearImmediate(immediate: Immediate) {
    clearImmediate(immediate);
    this._immediates.delete(immediate);
  }
}

