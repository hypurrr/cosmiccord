import { Shard, ShardStatus } from '../Shard';
import { Packet } from '../Gateway';
import DiscordEvent from '../../../Constants/DiscordEvent';
import { Guild } from '../../../Models/Guild/Guild';

export class GuildCreate {
  public static async emit(shard: Shard, packet: Packet) {
    let newGuild = false;
    if (!await shard.guilds.has(packet.d.id)) newGuild = true;
    const guild = new Guild(shard.client, packet.d);
    await shard.client.guilds.set(packet.d.id, packet.d);
    await shard.guilds.set(packet.d.id, packet.d);
    shard.guildLength--;
    if (shard.guildLength === 0 && shard.status !== ShardStatus.connected) {
      if (shard.status === ShardStatus.reconnecting) {
        shard.startTime = Date.now();
        shard.status = ShardStatus.connected;
        shard.shards.connected.set(shard.id, shard);
        shard.shards.set(shard.id, shard);

        for (const _ of shard.guilds) {
          shard.guilds.set(packet.d.id, packet.d);
          // eslint-disable-next-line no-await-in-loop
          await shard.client.guilds.set(packet.d.id, packet.d);
        }
        shard.client.emit(DiscordEvent.SHARD_READY, shard);
        return null;
      }
      shard.startTime = Date.now();
      shard.shards.connected.set(shard.id, shard);
      shard.shards.set(shard.id, shard);
      shard.status = ShardStatus.connected;
      shard.client.emit(DiscordEvent.SHARD_READY, shard);

      if (shard.shards.connected.size === shard.shards.shardCount) {
        shard.startTime = Date.now();
        shard.status = ShardStatus.connected;
        return shard.client.emit(DiscordEvent.READY);
      }
    }
    return shard.client.emit(newGuild ? DiscordEvent.GUILD_CREATE : DiscordEvent.GUILD_AVAILABLE, guild);
  }
}
