import { Shard } from '../Shard';
import DiscordEvent from '../../../Constants/DiscordEvent';
import { Message } from '../../../Models/Message';

export class MessageCreate {
  static emit(shard: Shard, packet: Record<string, any>) {
    const message = new Message(shard.client, packet.d);
    setTimeout(async () => {
      const user = message.author;
      user.lastMessageID = packet.d.id;
      user.lastMessageChannelID = packet.d.channel_id;
      await shard.client.users.set(user.id, user);
      await shard.client.emit(DiscordEvent.MESSAGE_CREATE, message)
    }, 1); 
  }
}
