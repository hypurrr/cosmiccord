import { Shard, ShardStatus } from '../Shard';
import { Packet } from '../Gateway';
import { SelfUser } from '../../../Models/SelfUser';
import DiscordEvent from '../../../Constants/DiscordEvent';

export class Ready {
  public static async emit(shard: Shard, packet: Packet) {
    await shard.client.cache.set(`SELF:${packet.d.user.id}`, packet.d.user);
    shard.client.self = new SelfUser(shard.client, packet.d.user);
    shard.gateway.sessionID = packet.d.session_id;
    for (let i = 0; i < packet.d.guilds.length; i++) {
      // eslint-disable-next-line no-await-in-loop
      await shard.client.guilds.set(`${packet.d.guilds[i].id}`, packet.d.guilds[i]);
    }
    if (packet.d.guilds.length === 0) {
      shard.status = ShardStatus.connected;
      shard.shards.connected.set(shard.id, shard);
      if (shard.shards.connected.size === shard.shards.shardCount && shard.status !== ShardStatus.connected) {
        return shard.client.emit(DiscordEvent.READY);
      }
    }
    shard.gateway.ready = true;
    shard.gateway.connecting = false;
    shard.guildLength = packet.d.guilds.length;
    return null;
  }
}
