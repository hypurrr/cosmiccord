import { Shard, ShardStatus } from './Shard';
import EventEmitter from 'eventemitter3';
import Zlib, { Unzip } from 'zlib';
import { WebSocket } from '@clusterws/cws';
import DiscordEvent from '../../Constants/DiscordEvent';
import { GatewayOPCodes } from '../../Constants/OPCodes';
import { Ready } from './Events/Ready';
import { GuildCreate } from './Events/GuildCreate';
import { MessageCreate } from './Events/MessageCreate';

// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
export type Packet = Record<string, any>;
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
type OpCodes = number;
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
global.cws = {
  EventEmitter: EventEmitter,
};
// eslint-disable-next-line @typescript-eslint/ban-ts-ignore
// @ts-ignore
const Z_SYNC_FLUSH = Zlib.constants ? Zlib.constants.Z_SYNC_FLUSH : Zlib.Z_SYNC_FLUSH;
let ZlibSync: any;
(() => {
  try {
    ZlibSync = require('zlib-sync');
  } catch (err) {
    ZlibSync = null;
  }
})();

/**
 * Represents the Gateway of a Shard
 * @class {Gateway}
 * @param {Shard} shard The shard this Gateway is tied too
 * @private
 */
export class Gateway {
  public shard!: Shard;
  public seq?: number;
  public sessionID?: number;
  public startTime = Date.now();
  public ws?: WebSocket;
  public ready = false;
  public connecting = false;
  public reconnectInterval = 1000;
  public connectAttempts = 0;
  public lastHeartbeatAck = true;
  public lastHeartbeatReceived: any = null;
  public lastHeartbeatSent: any = null;
  public heartbeatInterval: any = null;
  public latency = 0;
  public pings: Array<number> = [];
  private fromReconnect = false;
  public _zlibSync: any;
  public _zlibIncomingChunks: any;
  public _zlibChunks: any;
  public _zlib?: Unzip;
  public _zlibFlushing: any;
  public constructor(shard: Shard) {
    Object.defineProperty(this, 'shard', { value: shard });
    /**
     * Contains the rate limit queue and metadata
     * @type {Object}
     * @private
     */
    Object.defineProperty(this, 'ratelimit', {
      value: {
        queue: [],
        total: 120,
        remaining: 120,
        time: 60e3,
        timer: null,
      },
    });
  }
  /**
   * Gets the shards current uptime
   * @type {number}
   * @readonly
   */
  public get uptime() {
    return this.startTime ? Date.now() - this.startTime : 0;
  }
  /**
   * Average heartbeat ping of the websocket, obtained by averaging the WebSocketShard#pings property
   * @type {number}
   * @readonly
   */
  public get ping() {
    const sum = this.pings.reduce((a, b) => a + b, 0);
    return sum / this.pings.length;
  }
  /**
   * Disconnect/Reconnects a shard
   * @param {boolean} [reconnect=false] Whether to reconnect the shard after it's death
   * @returns {Shard}
   */
  public disconnect(reconnect = false): Shard {
    if (!this.ws) return this.shard;
    if (this.heartbeatInterval) {
      clearInterval(this.heartbeatInterval);
      this.heartbeatInterval = null;
    }

    try {
      if (reconnect) {
        this.shard.client.emit(DiscordEvent.SHARD_RECONNECT, [Gateway, { id: this.shard.id }]);
        this.shard.status = ShardStatus.reconnecting;
        this.ws.terminate();
        this.ws = undefined;
        setTimeout(() => this.connect(true), this.shard.client.options.shard.loginDelay);
      } else {
        this.ws.close(1000);
      }
    } catch (err) {
      this.shard.client.emit(DiscordEvent.ERROR, [Gateway, new Error(err)]);
    }
    return this.shard;
  }

  public connect(reconnected = false) {
    if (reconnected) this.fromReconnect = true;
    this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { id: this.shard.id, message: `loading from reconnect: ${this.fromReconnect}` }]);
    this.shard.client.emit(DiscordEvent.SHARD_LOADING, [Gateway, { id: this.shard.id }]);
    this.shard.status = ShardStatus.connecting;
    this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { Shard: this.shard.id, message: 'Initialising Gateway Connection' }]);
    this._init();
    return reconnected;
  }

  private _init() {
    try {
      if (this.shard.client.options.ws.compress && ZlibSync !== null) {
        this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, {
          shard: this.shard.id,
          message: 'pako/zlib-sync detected, creating Inflate instance',
        }]);
        this._zlibSync = new ZlibSync.Inflate({ chunkSize: 128 * 1024 } as any);
      }
      /**
       * The WebSocket that connects to the Gateway
       * @type {WebSocket}
       */
      this.ws = new WebSocket(this.shard.shards.gatewayURL);
      this.ws.on('open', () => {
        this.shard.client.shardID = this.shard.id;
        this.shard.client.emit(DiscordEvent.DEBUG, [
          Gateway,
          {
            shard: this.shard.id,
            message: `Connected to Gateway ${this.shard.shards.gatewayURL} using token ${this.shard.client.token}`,
          },
        ]);
      });
      this.ws.on('message', (data: any) => {
        try {
          if (data instanceof ArrayBuffer) {
            if (this.shard.client.options.ws.compress) {
              data = Buffer.from(data);
            }
          } else if (Array.isArray(data)) {
            data = Buffer.concat(data);
          }

          if (this.shard.client.options.ws.compress && ZlibSync) {
            if (data.length >= 4 && data.readUInt32BE(data.length - 4) === 0xFFFF) {
              this._zlibSync.push(data, Z_SYNC_FLUSH);
              if (this._zlibSync.err) {
                this.shard.client.emit(DiscordEvent.ERROR, [Gateway, { shard: this.shard.id, error: new Error(`Zlib error ${this._zlibSync.err}: ${this._zlibSync.msg}`) }]);
                return null;
              }
              data = Buffer.from(this._zlibSync.result);
              return this._listenMessage(JSON.parse(data));
            } else {
              this._zlibSync.push(data, false);
            }
            return null;
          } else {
            return this._listenMessage(JSON.parse(data));
          }
        } catch (err) {
          return this.shard.client.emit(DiscordEvent.ERROR, [Gateway, { shard: this.shard.id, error: err }]);
        }
      });
      // eslint-disable-next-line complexity
      this.ws.on('close', (event: any) => {
        let err = !event.code || event.code === 1000 ? null : new Error(`${event.code}: ${event.reason}`);
        let reconnect = true;
        if (event.code) {
          this.shard.client.emit(DiscordEvent.DEBUG, [
            Gateway,
            {
              shard: this.shard.id,
              message: `${event.code === 1000 ? 'Clean' : 'Unclean'} WS close: ${event.code}: ${event.reason}`,
            },
          ]);
          switch (event.code) {
            case 4001: {
              err = new Error('[4001]: Gateway received invalid OP code');
              break;
            }
            case 4002: {
              err = new Error('[4002]: Gateway received invalid message');
              break;
            }
            case 4003: {
              err = new Error('[4003]: Not authenticated');
              break;
            }
            case 4004: {
              err = new Error('[4004]: Authentication failed');
              reconnect = false;
              this.shard.client.emit(DiscordEvent.ERROR, [Gateway, new Error(`Invalid Token: ${this.shard.client.token}`)]);
              break;
            }
            case 4005: {
              err = new Error('[4005]: Already authenticated');
              break;
            }
            case 4006 || 4009: {
              err = new Error(`[${event.code}]: Invalid Session`);
              this.sessionID = undefined;
              break;
            }
            case 4007: {
              err = new Error(`[4007]: Invalid sequence number: ${this.seq}`);
              this.seq = 0;
              break;
            }
            case 4008: {
              err = new Error('[4008]: Gateway connection was rate-limited');
              break;
            }
            case 4010: {
              err = new Error('[4010]: Invalid shard key');
              break;
            }
            case 4011: {
              err = new Error('[4011]: Shard has too many guilds (>2500)');
              reconnect = false;
              break;
            }
            case 1006: {
              err = new Error('[1006]: Connection reset by peer');
              break;
            }
            default: {
              if (!event.wasClean && event.reason) {
                err = new Error(`[${event.code}]: ${event.reason}`);
              }
            }
          }
          this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, error: err }]);
        } else {
          this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: `WebSocket close: Unknown code: ${event.reason}` }]);
        }
        this.disconnect(reconnect);
        if (this.shard.shards.connected.has(this.shard.id)) this.shard.shards.connected.delete(this.shard.id);
        this.shard.status = ShardStatus.disconnected;
        if (this.shard.shards.connected.size === 0) {
          this.shard.client.emit(DiscordEvent.SHARD_DISCONNECT_ALL, { message: 'All Shards have been disconnected!' });
        }
        this.shard.client.emit(DiscordEvent.SHARD_DISCONNECT, {
          id: this.shard.id,
          message: `Shard disconnected with code ${event.code || 'unknown'}`,
          reason: event.reason || 'unknown',
        });
      });
      this.ws.on('error', (err: any) => {
        this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, `Error connecting to the Gateway: ${err.stack}`]);
        this.shard.client.emit(DiscordEvent.ERROR, [
          Gateway,
          {
            id: this.shard.id,
            error: err,
          },
        ]);
      });
      return this;
    } catch (err) {
      this.shard.client.emit(DiscordEvent.ERROR, [Gateway, err]);
      process.exit(1);
      throw err;
    }
  }

  /**
   * The Discord opcodes, Refer to [Opcodes and Status Codes](https://discordapp.com/developers/docs/topics/opcodes-and-status-codes#gateway-opcodes)
   * @typedef {number} OpCodes
   */

  /**
   * Data that can be resolved to give a permission number. This can be:
   * @typedef {Object} Packet
   * @property {OpCodes} op The opcode for the payload returned by Discord
   * @property {number?} s sequence number, used for resuming sessions and heartbeats, only on opcode 0
   * @property {string?} t The event emitted from Discord, only on opcode 0
   * @property {*} d The event data emitted from Discord
   */

  /**
   * Handles the messages sent by the Gateway
   * @param {Packet} packet The {@link Packet} received from Discord.
   * @private
   */
  private _listenMessage(packet: Packet) {
    switch (packet.op) {
      case GatewayOPCodes.HELLO: {
        this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: 'Received OpCode 10 (hello) from Discord!' }]);
        this.startHeartbeat(packet.d.heartbeat_interval);
        break;
      }
      case GatewayOPCodes.EVENT: {
        this.seq = packet.s;
        this.listenEvent(packet);
        this.shard.client.emit(DiscordEvent.RAW, packet);
        break;
      }
      case GatewayOPCodes.HEARTBEAT_ACK: {
        this.lastHeartbeatReceived = new Date().getTime();
        this.latency = this.lastHeartbeatReceived - this.lastHeartbeatSent;
        this.pings.unshift(this.latency);
        if (this.pings.length > 3) this.pings.length = 3;
        break;
      }
      case GatewayOPCodes.INVALID_SESSION: {
        if (!packet.d) {
          this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: 'Received OpCode 9 (Invalid Session). Will re-login.' }]);
          this.identify();
        } else {
          this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: 'Received OpCode 9 (Invalid Session). Will resume.' }]);
          this.disconnect();
          this.ws = new WebSocket(this.shard.shards.gatewayURL);
          this.ws.on('open', () => this.resume());
        }
        break;
      }
      case GatewayOPCodes.RECONNECT: {
        this.disconnect(true);
        break;
      }
      default: {
        this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: `Received Unhandled OpCode: ${packet.op}` }]);
      }
    }
  }

  /**
   * Listens for Discord events from the Gateway
   * @param {Packet} packet The packet received from Discord
   */
  private async listenEvent(packet: Packet) {
    if (this.shard.client.options.ws.ignoredEvents.includes(packet.t)) return;
    switch (packet.t) {
      case DiscordEvent.READY: {
        await Ready.emit(this.shard, packet);
        break;
      }
      case DiscordEvent.GUILD_CREATE: {
        await GuildCreate.emit(this.shard, packet);
        break;
      }
      case DiscordEvent.MESSAGE_CREATE: {
        await MessageCreate.emit(this.shard, packet);
        break;
      }
      default: {
        this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: `Unhandled gateway event: ${packet.t}` }]);
        break;
      }
    }
  }

  private identify() {
    const largeThreshold = this.shard.client.options.ws.largeThreshold > 250 || this.shard.client.options.ws.largeThreshold < 50 ? (() => {
      this.shard.client.emit(DiscordEvent.ERROR, [
        Gateway,
        `options.ws.largeThreshold value "${this.shard.client.options.ws.largeThreshold}" must be between 50 and 250, defaulting to 250`,
      ]);
      return 250;
    })() : this.shard.client.options.ws.largeThreshold;
    const identify: Packet = {
      op: 2,
      d: {
        token: this.shard.client.token,
        properties: {
          $os: process.platform,
          $browser: 'CosmicCord',
          $device: 'CosmicCord',
        },
        // eslint-disable-next-line @typescript-eslint/camelcase
        large_threshold: largeThreshold,
      },
    };
    if (this.shard.client.options.shard.count > 1) {
      identify.shard = [this.shard.id, this.shard.shards.shardCount];
    }
    this.send(identify);
  }

  /**
   * Sends an Opcode 6 ( Resume ) to Discord
   */
  private resume() {
    this.send({
      op: GatewayOPCodes.RESUME,
      d: {
        token: this.shard.client.token,
        // eslint-disable-next-line @typescript-eslint/camelcase
        session_id: this.sessionID,
        seq: this.seq,
      },
    });
  }

  private startHeartbeat(interval: number) {
    // Send the first heartbeat
    this.send({ op: 1, d: { seq: this.seq } });

    // Get the time the last heartbeat was sent in ms
    this.lastHeartbeatSent = new Date().getTime();

    // Debugger #1
    this.shard.client.emit(DiscordEvent.DEBUG, [Gateway, { shard: this.shard.id, message: 'Sent the first Heartbeat to Discord! Starting Interval...' }]);

    // Send an identify payload
    this.identify();

    // Debugger #2
    this.shard.client.emit(DiscordEvent.DEBUG, [
      Gateway,
      {
        shard: this.shard.id,
        message: `Sent an Opcode 2 (Identify) to Discord! With token: ${this.shard.client.token}`,
      },
    ]);

    // Let's create an interval here for sending heartbeats
    this.heartbeatInterval = setInterval(() => {
      this.send({ op: GatewayOPCodes.HEARTBEAT, d: { seq: this.seq } });

      // Get the time the last heartbeat was sent in ms
      this.lastHeartbeatSent = new Date().getTime();

      // Debugger #3
      this.shard.client.emit(DiscordEvent.DEBUG, [
        Gateway,
        {
          shard: this.shard.id,
          message: 'Sent another Heartbeat to Discord!',
        },
      ]);
    }, interval);

    return this;
  }

  public send(data: any) {
    if (this.ws) this.ws.send(typeof data === 'object' ? JSON.stringify(data) : data);
    return data;
  }
}
