import { Client } from '../Client';
import EventEmitter from 'eventemitter3';
import { Gateway } from './Gateway';
import { GatewayOPCodes } from '../../Constants/OPCodes';
import { ShardManager } from './ShardManager';
import { ActivityInterface } from '../../Interfaces/ActivityInterface';

export class Shard extends EventEmitter {
  public client!: Client;
  public shards!: ShardManager;
  public id: number;
  public guilds: Map<string, any>;
  public ready = false;
  public status: ShardStatus = 0;
  public gateway: Gateway;
  public guildLength = 0;
  public startTime?: number;
  public constructor(id: number, client: Client) {
    super();
    Object.defineProperty(this, 'client', { value: client });
    Object.defineProperty(this, 'shards', { value: client.shards });

    this.id = id;
    this.guilds = new Map();
    this.gateway = new Gateway(this);
  }
  public connect() {
    this.gateway.connect();
  }

  /**
   * Sends an Activity Update to the Gateway
   * @param {ActivityInterface} [options] The options for the Activity
   * @returns {ActivityInterface}
   */
  public sendActivity(options: ActivityInterface = {}) {
    this.gateway.send({
      op: GatewayOPCodes.STATUS_UPDATE,
      d: {
        game: options.game || this.client.presence.game,
        since: options.since || this.client.presence.since,
        afk: Boolean(options.afk) || this.client.presence.afk,
        status: options.status || this.client.presence.status,
      },
    });

    return options;
  }
}

export enum ShardStatus {
  disconnected,
  connecting,
  connected,
  reconnecting,
  loading,
}
