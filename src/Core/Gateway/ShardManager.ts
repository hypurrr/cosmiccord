import { Shard } from './Shard';
import { Client } from '../Client';
import DiscordEvent from '../../Constants/DiscordEvent';
import Endpoints from '../Rest/Endpoints';

export class ShardManager extends Map<number, Shard> {
  public client!: Client;
  public gatewayURL!: string;
  public shardCount: number;
  public connected: Map<number, Shard> = new Map();
  public constructor(client: Client) {
    super();
    Object.defineProperty(this, 'client', { value: client, enumerable: false });
    this.shardCount = this.client.options.shard.maxShards;
  }

  public spawn(id: number) {
    let shard = this.get(id);
    try {
      if (!shard) {
        shard = new Shard(id, this.client);
        this.set(id, shard as Shard);
        shard.connect();
      } else {
        shard.connect();
      }
    } catch (err) {
      this.client.emit(DiscordEvent.ERROR, [ShardManager, err.message]);
      throw err;
    }
  }

  public async _init() {
    this.client.emit(DiscordEvent.DEBUG, [ShardManager, 'Initiating Shard Manager']);
    const maxShardsAuto = this.client.options.shard.maxShards === 'auto';
    const lastShardIDUndefined = typeof this.client.options.shard.lastShardID === 'undefined';

    const data = await (maxShardsAuto ?
      this.client.rest.request('get', Endpoints.GATEWAY_BOT) :
      this.client.rest.request('get', Endpoints.GATEWAY));

    if (maxShardsAuto) {
      if (!data.shards) {
        this.client.emit(DiscordEvent.ERROR, [ShardManager, new Error('Failed to auto-shard, Lack of shard data provided by Discord.')]);
        process.exit(1);
      }
      this.client.emit(DiscordEvent.DEBUG, [ShardManager, 'Preparing to auto-shard']);
      this.shardCount = data.shards;
      this.client.options.shard.maxShards = data.shards;
      if (lastShardIDUndefined) this.client.options.shard.lastShardID = data.shards - 1;
    } else if (lastShardIDUndefined) {
      this.client.options.shard.lastShardID = this.shardCount === 0 ? 0 : this.shardCount - 1;
    }

    this.gatewayURL = `${data.url}/?v=${this.client.options.ws.gateway.version}&encoding=json`;
    if (this.client.options.ws.compress) this.gatewayURL += '*compress=zlib-stream';
    this.client.emit(DiscordEvent.DEBUG, [ShardManager, { shard: 'Global', message: `Connecting to Discord, Shards: ${this.shardCount}` }]);
    for (let i = this.client.options.shard.firstShardID; i <= this.client.options.shard.lastShardID; ++i) {
      setTimeout(() => this.spawn(i), i * this.client.options.shard.loginDelay);
    }
  }
}
