import https from 'https';
import fetch from 'node-fetch';
import { RestManager } from './RestManager';
import { Client } from '../Client';
import { version } from '../../index';

let agent: any;
if (https.Agent) agent = new https.Agent({ keepAlive: true });

export class APIRequest {
  public rest!: RestManager;
  public client!: Client;
  public method: string;
  public route: string;
  public options: any;
  public path: string
  public constructor(rest: RestManager, method: string, path: string, options: any) {
    Object.defineProperty(this, 'rest', { value: rest });
    Object.defineProperty(this, 'client', { value: rest.client });
    this.method = method;
    this.route = options.route || path;
    this.options = options;

    let queryString = '';
    if (options.query) {
      // Filter out undefined query options
      const query = Object.entries(options.query).filter(([, value]) => value !== null && typeof value !== 'undefined');
      queryString = new URLSearchParams(query as any).toString();
    }
    this.path = `${path}${queryString && `?${queryString}`}`;
  }
  public make() {
    const API = this.options.versioned === false ? this.client.options.rest.api : `${this.client.options.rest.api}/v${this.client.options.rest.version}`;
    const url = API + this.path;
    let headers: {[key: string]: string} = {
      Authorization: `${this.client.options.notbot ? '' : 'Bot '}${this.client.token}`,
      'Content-Type': 'application/json',
      'User-Agent': `CosmicCord ${version}`,
      // # 'Accept-Encoding': 'gzip,deflate',
    };
    if (this.options.reason) headers['X-Audit-Log-Reason'] = encodeURIComponent(this.options.reason);
    if (this.options.headers) headers = Object.assign(headers, this.options.headers);

    let body;
    if (this.options.files) {
      body = new FormData();
      for (const file of this.options.files) if (file && file.file) body.append(file.name, file.file, file.name);
      if (typeof this.options.data !== 'undefined') body.append('payload_json', JSON.stringify(this.options.data));
    } else if (this.options.data != null) { // eslint-disable-line eqeqeq
      body = JSON.stringify(this.options.data);
      headers['Content-Type'] = 'application/json';
    }
    return fetch(url, {
      method: this.method.toUpperCase(),
      headers: headers,
      // eslint-disable-next-line @typescript-eslint/ban-ts-ignore
      // @ts-ignore
      agent,
      body: body as any,
    });
  }
}