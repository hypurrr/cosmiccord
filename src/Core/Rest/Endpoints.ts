import { Snowflake } from '../../Utils/Snowflake';

export type TAllowedImageFormats = 'webp' | 'png' | 'jpg' | 'gif';
const AllowedImageFormats = [
  'webp',
  'png',
  'jpg',
  'gif',
];

const AllowedImageSizes = Array.from({ length: 8 }, (_, i) => 2 ** (i + 4));

function makeImageUrl(_: string, { format = 'webp', size = 256 } = {}) {
  if (format && !AllowedImageFormats.includes(format)) throw new Error(`IMAGE_FORMAT ${format}`);
  if (size && !AllowedImageSizes.includes(size)) throw new RangeError(`IMAGE_SIZE, ${size}`);
  return `.${format}${size ? `?size=${size}` : ''}`;
}
/**
 * Options for Image URLs.
 * @typedef {Object} ImageURLOptions
 * @property {string} [format] One of `webp`, `png`, `jpg`, `gif`. If no format is provided,
 * it will be `gif` for animated avatars or otherwise `webp`
 * @property {number} [size] One of `16`, `32`, `64`, `128`, `256`, `512`, `1024`, `2048`
 */

export default {
  GATEWAY: '/gateway',
  GATEWAY_BOT: '/gateway/bot',
  /* CDN */
  CDN(defaultImageFormat: string) {
    return {
      Emoji: (emojiID: Snowflake, format = 'png') => `/emojis/${emojiID}.${format}`,
      Asset: (name: string) => `/assets/${name}`,
      DefaultAvatar: (discriminator: number) => `/embed/avatars/${discriminator % 5}.png`,
      Avatar: (userID: Snowflake, hash: string, format = 'default', size: number) => {
        if (format === 'default') format = hash.startsWith('a_') ? 'gif' : defaultImageFormat;
        return makeImageUrl(`/avatars/${userID}/${hash}`, { format, size });
      },
      Banner: (guildID: Snowflake, hash: string, format = defaultImageFormat, size: number) =>
        makeImageUrl(`/banners/${guildID}/${hash}`, { format, size }),
      GuildIcon: (guildID: Snowflake, hash: string, format = defaultImageFormat, size: number) =>
        makeImageUrl(`/icons/${guildID}/${hash}`, { format, size }),
      AppIcon: (clientID: Snowflake, hash: string, format = defaultImageFormat, size: number) =>
        makeImageUrl(`/app-icons/${clientID}/${hash}`, { size, format }),
      AppAsset: (clientID: Snowflake, hash: string, format = defaultImageFormat, size: number) =>
        makeImageUrl(`/app-assets/${clientID}/${hash}`, { size, format }),
      GDMIcon: (channelID: Snowflake, hash: string, format = defaultImageFormat, size: number) =>
        makeImageUrl(`/channel-icons/${channelID}/${hash}`, { size, format }),
      Splash: (guildID: Snowflake, hash: string, format = defaultImageFormat, size: number) =>
        makeImageUrl(`/splashes/${guildID}/${hash}`, { size, format }),
    };
  },
  DEFAULT_AVATARS: (discriminator: number) => `/embed/avatars/${discriminator % 5}`,
  AVATARS: (userID: string, avatar: string) => `/avatars/${userID}/${avatar}`,
  GUILD_ICONS: (guildID: string, icon: string) => `/icons/${guildID}/${icon}`,

  /* User */
  USER: (userID: string) => `/users/${userID}`,
  USER_CHANNELS: (userID: string) => `/users/${userID}/channels`,
  USER_GUILD: (guildID: string) => `/users/@me/guilds/${guildID}`,
  USER_ME: `/users/@me`,
  USERS: `/users`,

  /* Channels */
  CHANNEL: (channelID: string) => `/channels/${channelID}`,
  CHANNEL_PERMISSION: (channelID: string, userOrRoleID: string) => `/channels/${channelID}/permissions/${userOrRoleID}`,
  CHANNEL_REACTION: (channelID: string, messageID: string, emoji: string, user: string) => `/channels/${channelID}/messages/${messageID}/reactions/${emoji}/${user}`,
  CHANNEL_REACTIONS: (channelID: string, messageID: string) => `/channels/${channelID}/messages/${messageID}/reactions`,
  CHANNEL_INVITES: (channelID: string) => `/channels/${channelID}/invites`,
  CHANNEL_MESSAGE: (channelID: string, messageID: string) => `/channels/${channelID}/messages/${messageID}`,
  CHANNEL_MESSAGES: (channelID: string) => `/channels/${channelID}/messages`,
  CHANNEL_BULKDELETE: (channelID: string) => `/channels/${channelID}/messages/bulk-delete`,
  CHANNEL_PIN_MESSAGES: (channelID: string, messageID: string) => `/channels/${channelID}/pins/${messageID}`,
  CHANNEL_PINNED_MESSAGES: (channelID: string) => `/channels/${channelID}/pins`,
  CHANNEL_WEBHOOKS: (channelID: string) => `/channels/${channelID}/webhooks`,

  /* Guild */
  GUILDS: `/guilds`,
  GUILD: (guildID: string) => `/guilds/${guildID}`,
  GUILD_MEMBER_NICK: (guildID: string, userID: string) => `/guilds/${guildID}/members/${userID}/nick`,
  GUILD_BAN: (guildID: string, userID: string) => `/guilds/${guildID}/bans/${userID}`,
  GUILD_BANS: (guildID: string) => `/guilds/${guildID}/bans`,
  GUILD_CHANNEL: (guildID: string, channelID: string) => `/guilds/${guildID}/channels/${channelID}`,
  GUILD_CHANNELS: (guildID: string) => `/guilds/${guildID}/channels`,
  GUILD_INVITES: (guildID: string) => `/guilds/${guildID}/invites`,
  GUILD_MEMBER: (guildID: string, userID: string) => `/guilds/${guildID}/members/${userID}`,
  GUILD_MEMBERS: (guildID: string) => `/guilds/${guildID}/members`,
  GUILD_REGIONS: (guildID: string) => `/guilds/${guildID}/regions`,
  GUILD_PRUNE: (guildID: string) => `/guilds/${guildID}/prune`,
  GUILD_ROLES: (guildID: string) => `/guilds/${guildID}/roles/`,
  GUILD_ROLE: (guildID: string, roleID: string) => `/guilds/${guildID}/roles/${roleID}`,
  GUILD_MEMBER_ROLE: (guildID: string, userID: string, roleID: string) => `/guilds/${guildID}/members/${userID}/roles/${roleID}`,
};
