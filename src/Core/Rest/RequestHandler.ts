import { RestManager } from './RestManager';
import { APIRequest } from './APIRequest';
import DiscordEvent from '../../Constants/DiscordEvent';
import DiscordAPIError, { HTTPError } from './RestErrors';

export class RequestHandler {
  private manager!: RestManager;
  public busy: boolean;
  public queue: any[];
  public reset: number;
  public remaining: number;
  public limit: number;
  public retryAfter: number;
  public constructor(manager: RestManager) {
    Object.defineProperty(this, 'manager', { value: manager });
    this.busy = false;
    this.queue = [];
    this.reset = -1;
    this.remaining = -1;
    this.limit = -1;
    this.retryAfter = -1;
  }
  public static parseResponse(res: any) {
    if (res.headers.get('content-type').startsWith('application/json')) return res.json();
    return res.buffer();
  }

  public static getAPIOffset(serverDate: number): number {
    return new Date(serverDate).getTime() - Date.now();
  }

  public static calculateReset(reset: number, serverDate: number): number {
    return new Date(Number(reset) * 1000).getTime() - RequestHandler.getAPIOffset(serverDate);
  }

  public push(request: { request: APIRequest; resolve: (value?: unknown) => void; reject: (value?: unknown) => void; retries: number }) {
    if (this.busy) {
      this.queue.push(request);
      return this.run();
    } else {
      return this.execute(request);
    }
  }

  public run(): Promise<any> {
    if (this.queue.length === 0) return Promise.resolve();
    return this.execute(this.queue.shift());
  }

  public get limited(): boolean {
    return (this.manager.globalTimeout || this.remaining <= 0) && Date.now() < this.reset;
  }

  public get _inactive(): boolean {
    return this.queue.length === 0 && !this.limited && !this.busy;
  }

  public async execute(item: any): Promise<any> {
    if (this.busy) {
      this.queue.unshift(item);
      return null;
    }

    this.busy = true;
    const { reject, request, resolve } = item;
    if (this.limited) {
      const timeout = this.reset + this.manager.client.options.restTimeOffset - Date.now();

      if (this.manager.client.listenerCount(DiscordEvent.RATE_LIMIT)) {
        this.manager.client.emit(DiscordEvent.RATE_LIMIT, [RequestHandler, {
          timeout,
          limit: this.limit,
          method: request.method,
          oath: request.path,
          route: request.route,
        }]);
      }

      if (this.manager.globalTimeout) {
        await this.manager.globalTimeout;
      } else {
        // Wait for the timeout to expire in order to avoid an actual 429
        await new Promise(_ => setTimeout(_, timeout));
      }
    }
    const start = Date.now();
    let res;
    try {
      res = await request.make();
    } catch (error) {
      this.busy = false;
      return reject(new HTTPError(
        error.message,
        error.constructor.name,
        error.status,
        request.method,
        request.path,
      ));
    }

    if (res && res.headers) {
      const serverDate = res.headers.get('date');
      const limit = res.headers.get('x-ratelimit-limit');
      const remaining = res.headers.get('x-ratelimit-remaining');
      const reset = res.headers.get('x-ratelimit-reset');
      const retryAfter = res.headers.get('retry-after');

      this.limit = limit ? Number(limit) : Infinity;
      this.remaining = remaining ? Number(remaining) : 1;
      this.reset = reset ? RequestHandler.calculateReset(reset, serverDate) : Date.now();
      this.retryAfter = retryAfter ? Number(retryAfter) : -1;

      // https://github.com/discordapp/discord-api-docs/issues/182
      if (item.request.route.includes('reactions')) {
        this.reset = new Date(serverDate).getTime() - RequestHandler.getAPIOffset(serverDate) + 250;
      }

      // Handle global ratelimit
      if (res.headers.get('x-ratelimit-global')) {
        // Set the manager's global timeout as the promise for other requests to "wait"
        this.manager.globalTimeout = await new Promise(_ => setTimeout(_, this.retryAfter));

        // Wait for the global timeout to resolve before continuing
        await this.manager.globalTimeout;

        // Clean up global timeout
        this.manager.globalTimeout = null;
      }
    }

    this.busy = false;

    if (res.ok) {
      const success = await RequestHandler.parseResponse(res);
      // Nothing wrong with the request, proceed with the next one
      resolve(success);
      this.manager.client.emit(DiscordEvent.DEBUG, [
        RequestHandler,
        `[${this.manager.client.shardID}] Request > [${request.method}] "${request.path}": { status: ${res.status}, time: ${Date.now() - start} }`,
      ]);
      return this.run();
    } else if (res.status === 429) {
      // A ratelimit was hit - this should never happen
      this.queue.unshift(item);
      this.manager.client.emit(DiscordEvent.DEBUG, [RequestHandler, `429 hit on route ${item.request.route}`]);
      await new Promise(_ => setTimeout(_, this.retryAfter));
      return this.run();
    } else if (res.status >= 500 && res.status < 600) {
      // Retry the specified number of times for possible serverside issues
      if (item.retries === this.manager.client.options.retryLimit) {
        return reject(
          new HTTPError(res.statusText, res.constructor.name, res.status, item.request.method, request.path)
        );
      } else {
        item.retries++;
        this.queue.unshift(item);
        return this.run();
      }
    } else {
      // Handle possible malformed requests
      try {
        const data = await RequestHandler.parseResponse(res);
        if (res.status >= 400 && res.status < 500) {
          return reject(new DiscordAPIError(request.path, data, request.method, res.status));
        }
        return null;
      } catch (err) {
        return reject(
          new HTTPError(err.message, err.constructor.name, err.status, request.method, request.path)
        );
      }
    }
  }
}
