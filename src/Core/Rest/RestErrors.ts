/**
 * Represents a HTTP error from a request.
 * @extends Error
 */
export class HTTPError extends Error {
  public code: number;
  public method: string;
  public path: string;
  public constructor(message: string, className: string, code: number, method: string, path: string) {
    super(message);
    /**
     * The name of the error
     * @type {string}
     */
    this.name = className;

    /**
     * HTTP error code returned from the request
     * @type {number}
     */
    this.code = code || 500;

    /**
     * The HTTP method used for the request
     * @type {string}
     */
    this.method = method;

    /**
     * The path of the request relative to the HTTP endpoint
     * @type {string}
     */
    this.path = path;
  }
}
/**
 * Represents an error from the Discord API.
 * @extends Error
 */
export default class DiscordAPIError extends Error {
  public method: string;
  public path: string;
  public code: number;
  public httpStatus: number;
  public constructor(path: string, error: any, method: string, status: number) {
    super();
    const flattened = DiscordAPIError.flattenErrors(error.errors || error).join('\n');
    this.name = 'DiscordAPIError';
    this.message = error.message && flattened ? `${error.message}\n${flattened}` : error.message || flattened;

    /**
     * The HTTP method used for the request
     * @type {string}
     */
    this.method = method;

    /**
     * The path of the request relative to the HTTP endpoint
     * @type {string}
     */
    this.path = path;

    /**
     * HTTP error code returned by Discord
     * @type {number}
     */
    this.code = error.code;

    /**
     * The HTTP status code
     * @type {number}
     */
    this.httpStatus = status;
  }

  /**
   * Flattens an errors object returned from the API into an array.
   * @param {Object} obj Discord errors object
   * @param {string} [key] Used internally to determine key names of nested fields
   * @returns {string[]}
   * @private
   */
  public static flattenErrors(obj: any, key = ''): any {
    let messages: any = [];

    for (const [k, v] of Object.entries(obj)) {
      if (k === 'message') continue;
      const newKey = key ? isNaN(parseInt(k)) ? `${key}.${k}` : `${key}[${k}]` : k;

      if ((v as any)._errors) {
        messages.push(`${newKey}: ${(v as any)._errors.map((e: any) => e.message).join(' ')}`);
      } else if ((v as any).code || (v as any).message) {
        messages.push(`${(v as any).code ? `${(v as any).code}: ` : ''}${(v as any).message}`.trim());
      } else if (typeof v === 'string') {
        messages.push(v);
      } else {
        messages = messages.concat(this.flattenErrors(v, newKey));
      }
    }

    return messages;
  }
}
