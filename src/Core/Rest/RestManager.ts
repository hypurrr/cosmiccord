import { Client } from '../Client';
import { RequestHandler } from './RequestHandler';
import { APIRequest } from './APIRequest';

export class RestManager {
  public client!: Client;
  public handlers: Map<any, any>;
  public globalTimeout: any;
  public constructor(client: Client) {
    Object.defineProperty(this, 'client', { value: client });4
    this.handlers = new Map();
    this.globalTimeout = null;

    if (client.options.rest.sweepInterval > 0) {
      client.setInterval(() => {
        for (const [x, y] of this.handlers.entries()) {
          if (y._inactive) this.handlers.delete(x);
        }
      }, client.options.rest.sweepInterval);
    }
  }

  public async push(handler: RequestHandler, apiRequest: APIRequest) {
    return await new Promise((resolve, reject) => {
      handler.push({
        request: apiRequest,
        resolve,
        reject,
        retries: 0,
      });
    });
  }

  public request(method: 'get'|'post'|'delete'|'patch'|'put', url: string, options: any = {}): Promise<any> {
    const apiRequest = new APIRequest(this, method.toUpperCase(), url, options);
    let handler = this.handlers.get(apiRequest.route);

    if (!handler) {
      handler = new RequestHandler(this);
      this.handlers.set(apiRequest.route, handler);
    }

    return this.push(handler, apiRequest);
}
}