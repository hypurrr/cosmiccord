export interface ActivityInterface {
  since?: number;
  game?: {
    name?: string;
    type?: number;
    url?: string;
  };
  status?: string;
  afk?: boolean;
}
