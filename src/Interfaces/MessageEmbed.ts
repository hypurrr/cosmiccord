export interface EmbedFooter {
  text: string;
  icon_url?: string;
  proxy_icon_url?: string;
}

export interface EmbedMedia {
  url?: string;
  proxy_url?: string;
  height?: number;
  width?: number;
}

export interface EmbedVideo {
  url?: string;
  height?: string;
  width?: string;
}

export interface EmbedProvider {
  name?: string;
  url?: string;
}

/**
 * Author field of the Embed
 * @typedef EmbedAuthor
 * @property {string} name The text field of the author
 * @property {string} url Used for adding a image to the author
 */
export interface EmbedAuthor {
  name?: string;
  url?: string;
  icon_url?: string;
  proxy_icon_url?: string;
}

export interface EmbedField {
  name: string;
  value: string;
  inline?: boolean;
}

/**
 * MessageEmbed interface, Used for type enforcing
 * @typedef {MessageEmbed}
 * @property {string?} type Type of the embed
 *
 */
export interface MessageEmbed {
  type?: string;
  title?: string;
  description?: string;
  url?: string;
  timestamp?: number;
  color?: number;
  footer?: EmbedFooter;
  image?: EmbedMedia;
  thumbnail?: EmbedMedia;
  video?: EmbedVideo;
  provider?: EmbedProvider;
  author?: EmbedAuthor;
  fields?: Array<EmbedField>;
}
