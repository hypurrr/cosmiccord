import { Client } from '..';

export class Channel {
  private readonly client: Client;
  public raw: Record<string, any>;
  public id: string;
  public name: string;
  constructor(client: Client, data: Record<string, any>) {
    this.client = client;
    this.raw = data;
    this.id = data.id;
    this.name = data.name;
  }
  return() {
    return [this.client, this.id];
  }
}
