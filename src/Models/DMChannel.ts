import { Channel } from './Channel';
import { User } from './User';

export class DMChannel extends Channel {
  constructor(user: User, data: Record<string, any>) {
    super(user.client, data);
  }
}
