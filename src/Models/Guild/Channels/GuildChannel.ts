import { Channel } from '../../Channel';
import { Guild } from '../Guild';

export class GuildChannel extends Channel {
  constructor(guild: Guild, data: Record<string, any>) {
    super(guild.client, data);
  }
}
