import { Client } from '../../index';
import { GuildChannelCache } from '../../Core/Caches/GuildChannelCache';

export class Guild {
  readonly client: Client;
  public raw: Record<string, any>;
  public id: string;
  public name: string;
  public channels: GuildChannelCache;
  constructor(client: Client, data: Record<string, any>) {
    this.client = client;
    this.raw = data;
    this.id = data.id;
    this.name = data.name;
    this.channels = new GuildChannelCache(client, this);

    for (let i = 0; i < data.channels.length; i++) this.channels.set(data.channels[i].id, data.channels[i]).catch(e => { throw e; });
  }
  return() {
    return [this.client, this.id];
  }
}
