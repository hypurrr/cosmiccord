import { Client } from "../Core/Client";
import { User } from "./User";
import { TextChannel } from "./Guild/Channels/TextChannel";
import { DMChannel } from "./DMChannel";

/**
 * The class for all Messages
 * @class message
 * @param {Client} client The client that received the message
 * @param {any} data The Data received from Discord
 */
export class Message {
  public client!: Client;
  public raw!: Record<string, any>;
  public activity?: Record<string, any>;
  public application?: Record<string, any>;
  public attachments: Record<string, any>;
  public author!: User;
  public channel!: TextChannel | DMChannel;
  public constructor(client: Client, data: Record<string, any>) {
    Object.defineProperty(this, 'client', { value: client, enumerable: false });
    Object.defineProperty(this, 'raw', { value: data, enumerable: false });
    (async () => {
      /**
      * The message author
      * @type {User}
      */
      this.author = await client.users.get(data.author.id);
      /**
      * The channel the message was sent in
      * @type {TextChannel|DMChannel}
      */
      if (data.guild_id) {
        this.channel = await (await client.guilds.get(data.guild_id)).channels.get(data.channel_id)
      } else {
        this.channel = await this.client.channels.get(data.channel_id);
      }
    })();
    /**
     * The users activity
     * @type {Object|null}
     */
    this.activity = data.activity || null;
    /**
     * The application of the user (these exist?)
     * @type {Object|null}
     */
    this.application = data.application || null;
    /**
     * An array of message attachments
     * @type {Object[]}
     */
    this.attachments = data.attachments;

  }
}