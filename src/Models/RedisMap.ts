export class RedisMap  {
  private map = new Map<string, string>();

  async get(key: string) {
    return this.map.get(key) || null;
  }

  async set(key: string, data: string) {
    return this.map.set(key, data) || null
  }

  async del(key: string) {
    try {
      return this.map.delete(key) ? 1 : 0
    } catch (err) {
      return null
    }
  }
  async keys() {
    return [ ...this.map.keys() ]
  }
}
