import { User } from './User';
import { Client } from '..';

/**
 * The user of the bot, extending from user carrying the same properties
 * @class SelfUser
 * @extends User
 * @param {Client} client The Client
 * @param {*} data The data returned from Discord
 */
export class SelfUser extends User {
  public email: string | null;
  public locale: string | null;
  public mfaEnabled: boolean;
  public verified: boolean;
  public constructor(client: Client, data: Record<string, any>) {
    super(client, data);
    /**
     * The email of the client (this will almost always be null)
     * @type {string|null}
     */
    this.email = data.email || null;
    /**
     * The current locale of the client (this will almost always be null)
     * @type {string|null}
     */
    this.locale = data.locale || null;
    /**
     * The 2fa status of the client (this will almost always be false)
     * @type {boolean}
     */
    this.mfaEnabled = data.mfa_enabled;
    /**
     * If the client user is verified (this will almost always be false)
     * @type {boolean}
     */
    this.verified = data.verified;
  }
}
