import { Client } from '..';
import Endpoints from '../Core/Rest/Endpoints';

export class User {
  public client!: Client;
  public raw!: Record<string, any>;
  public avatar: string;
  public bot: boolean;
  public discriminator: number;
  public id: string;
  public tag: string;
  public mention: string;
  public username: string;
  public lastMessageID?: string;
  public lastMessageChannelID?: string;
  constructor(client: Client, data: Record<string, any>) {
    Object.defineProperty(this, 'client', { value: client, enumerable: false });
    Object.defineProperty(this, 'raw', { value: data, enumerable: false });
    /**
    * The avatar hash of the user
    * @type {string|null}
    */
    this.avatar = data.avatar || null;
    /**
     * Whether the user is a bot or not
     * @type {boolean}
     */
    this.bot = Boolean(data.bot);
    /**
     * The discriminator of the user
     * @type {number}
     */
    this.discriminator = data.discriminator;
    /**
     * The id of the user
     * @type {string}
     */
    this.id = data.id;
    /**
     * The user's tag, A shortcut for joining username and discriminator
     * @type {string}
     */
    this.tag = `${data.username}#${data.discriminator}`;
    /**
     * A pre formed mention for the user
     * @type {string}
     */
    this.mention = `<@${this.id}>`;
    /**
     * The user's username
     * @type {string}
     */
    this.username = data.username;
    this.lastMessageID = undefined;
    this.lastMessageChannelID = undefined;
  }

  /**
   * Returns the users creation date
   * @type {number}
   */
  public get createdTimestamp() {
    return new Date((parseInt(this.id) / 4194304) + 1420070400000).getTime();
  }

  /**
   * Returns the default user avatar
   * @type {string}
   */
  public get defaultAvatarURL() {
    return `${this.client.options.rest.cdn}${Endpoints.DEFAULT_AVATARS(this.discriminator)}.png`;
  }

  /**
   * Returns a formed avatarURL
   * @param {options} options of
   * @param {string} options.format The format the image is in
   * @param {number} options.size The size of the image
   * @returns {string}
   */
  public avatarURL(options: { format?: string; size?: number } = {}) {
    if (this.avatar) {
      const base = `${this.client.options.rest.cdn}${Endpoints.AVATARS(this.id, (this.avatar as string))}`;
      return `${base}.${options.format || this.client.options.rest.defaultImageFormat}?size=${options.size || 512}`;
    } else {
      return this.defaultAvatarURL;
    }
  }

  /**
   * Turns a user object into a mention when used in a string
   * @returns {string}
   */
  public toString() {
    return this.mention;
  }
}
