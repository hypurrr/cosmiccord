import { Client, DiscordEvent } from '..';
import ReadLine from 'readline';

(async () => {
  const client = new Client('' as string, {
    rest: {
      version: 7,
      api: 'https://discordapp.com/api',
      cdn: 'https://cdn.discordapp.com',
      invite: 'https://discord.gg',
      timeout: 2000,
      defaultImageFormat: 'webp',
      rateLimiterOffset: 0,
      requestTimeout: 15000,
      latencyThreshold: 30000,
      sweepInterval: 0,
    },
    ws: {
      enabled: true,
      ignoredEvents: ['TYPING_START'],
      largeThreshold: 250,
      compress: false,
      gateway: {
        version: 6,
      },
    },
    shard: {
      maxShards: 'auto',
      firstShardID: 0,
      lastShardID: undefined,
      loginDelay: 7500,
      autoReconnect: 'auto',
    },
    disableRedis: false,
  });

  client.on(DiscordEvent.DEBUG, ([source, message]) => {
    console.debug(`[DEBUG] ${source.name} >  ${typeof message === 'object' ?
      `Shard: ${message.shard}, ${message.error ?
        `Error: ${message.error}` :
        `Message: ${message.message}`
      }` :
      message
    }`);
  });

  client.on(DiscordEvent.ERROR, ([source, message]) => {
    console.error(`[ERROR] ${source.name} >  ${typeof message === 'object' ?
      `Shard: ${message.shard}, ${message.error ?
        `Error: ${message.error}` :
        `Message: ${message.message}`
      }` :
      message
    }`);
  });

  const rl = ReadLine.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  client.on(DiscordEvent.READY, async () => {
    console.log('Impossible!');
    console.log(process.pid);
    rl.on('line', async input => {
      const f: () => Promise<string> = async () => await input;
      const res = await eval(await f());
      console.log(res);
    });
  });
/*
  client.on(DiscordEvent.MESSAGE_CREATE, async packet => {
    const msg = await new Message(client, packet.d);
    setTimeout(() => {
      console.log(msg);
    }, 1)
    if (packet.d.author.bot) return;
    if (packet.d.content === 'gay') await client.rest.request('post', Endpoints.CHANNEL_MESSAGES(packet.d.channel_id), { data: { content: 'no u' } });
    if (packet.d.content.startsWith('eval')) {
      if (packet.d.author && packet.d.author.id === '249467130108575745') {
        let args = packet.d.content.split(' ');
        args.shift()
        try {
          const exec = await eval(`(await() => {${args.join(' ')}})()`);
          await client.rest.request('post', Endpoints.CHANNEL_MESSAGES(packet.d.channel_id), { data: {
            content: 'Eval responce',
            embed: {
              description: `\`\`\`js\n${inspect(exec, { depth: 1 })}\n\`\`\``,
              color: 0x0000ff,
              footer: {
                text: 'Milo Eval'
              }
            }
          } });
        } catch (err) {
          await client.rest.request('post', Endpoints.CHANNEL_MESSAGES(packet.d.channel_id), { data: {
            content: 'Eval responce',
            embed: {
              description: `\`\`\`js\n${inspect(err, { depth: 1 })}\n\`\`\``,
              color: 0x0000ff,
              footer: {
                text: 'Milo Eval'
              }
            }
          } });
        }
      }
    }
  });
  */
  await client.connect();
})();
