import {
  EmbedAuthor, EmbedField,
  EmbedFooter,
  EmbedMedia,
  EmbedProvider,
  EmbedVideo,
  MessageEmbed,
} from '../Interfaces/MessageEmbed';

/**
 * Constructs a MessageEmbed dynamically
 * @typedef {EmbedConstructor}
 * @implements {MessageEmbed}
 */
export class EmbedConstructor implements MessageEmbed {
  type?: string;
  title?: string;
  description?: string;
  url?: string;
  timestamp?: number;
  color?: number;
  footer?: EmbedFooter;
  image?: EmbedMedia;
  thumbnail?: EmbedMedia;
  video?: EmbedVideo;
  provider?: EmbedProvider;
  author?: EmbedAuthor;
  fields?: Array<EmbedField>;
  public constructor(data: MessageEmbed = {}) {
    this.type = data.type as string;
    this.title = data.title;
    this.description = data.description;
    this.url = data.url;
    this.timestamp = data.timestamp;
    this.color = data.color;
    this.footer = data.footer;
    this.image = data.image;
    this.thumbnail = data.thumbnail;
    this.video = data.video;
    this.provider = data.provider;
    this.author = data.author;
    this.fields = data.fields || [];
  }
}
