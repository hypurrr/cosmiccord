import { Client } from './Core/Client';

import DiscordEvent from './Constants/DiscordEvent';
const { version } = require('../package.json');

export default Client;
export {
  Client,
  DiscordEvent,
  version,
};
