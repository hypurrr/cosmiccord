const version = Number(process.version.slice(1).split('.')[0]);

if (version < 10) throw new Error('Node v10.0.0 or higher is required. Update Node on your system.');
if (version > 12) throw new Error('Node v12.0.0 or higher has issues with the included modules. We recommend Node v10.0.0 for best support');

